const argv = require('./config/yargs').argv;
const colors = require('colors');

const porHacer = require('./por-hacer/por-hacer'); // trae todas las funciones del archivo

let comando = argv._[0];

switch (comando) {
	case 'crear':
		let tarea = porHacer.crear(argv.descripcion);
		console.log(tarea);
		break;

	case 'listar':
		//console.log(argv.listarCompletas,'ok');
		if (argv.listarCompletas) {
			let nuevoListado = porHacer.getListado().filter(
				(tarea) => tarea.completado == true
			);
			for (let tarea of nuevoListado) {
				console.log('======Por hacer========='.green);
				console.log(tarea.descripcion);
				console.log('Estado: ', tarea.completado);
				console.log('------------------------'.green);
			}
		} else {
			let listado = porHacer.getListado();
			for (let tarea of listado) {
				console.log('======Por hacer========='.green);
				console.log(tarea.descripcion);
				console.log('Estado: ', tarea.completado);
				console.log('------------------------'.green);
			}
		}

		break;

	case 'actualizar':
		let actualizado = porHacer.actualizar(argv.descripcion, argv.completado);
		console.log(actualizado);
		break;
	case 'borrar':
		let borrado = porHacer.deleteTarea(argv.descripcion);
		console.log(borrado);
		break;

	default:
		console.log('Este comando no es reconocido');
		break;
}
