const fs = require('fs'); //importamos libreria para guardar docuentos File System

let listadoPorHacer = [];

//guarda los objetos en el simulador de la BD
const saveDB = () => {
	let data = JSON.stringify(listadoPorHacer); //convierte a un json valido

	fs.writeFile(`db/data.json`, data, (error) => {
		if (error) throw new Error('No se pudo grabar', error);
	});
};

//carga los datos del documento
const cargarDB = () => {
	try {
		listadoPorHacer = require('../db/data.json'); //carga toda la inf. que tiene el archivo y lo convierte a json
		//console.log(listadoPorHacer);
	} catch (error) {
		listadoPorHacer = [];
	}
};

const crear = (descripcion) => {
	cargarDB();

	let porHacer = {
		descripcion,
		completado: false,
	};

	listadoPorHacer.push(porHacer); // añade uno o más elementos al final de un array

	saveDB();

	return porHacer;
};

const getListado = () => {
	cargarDB();
	return listadoPorHacer;
};

const actualizar = (descripcion, completado = true) => {
	cargarDB();

	//Dame el index de la posicion de la tarea si es que conincide con alguna del array
	//esta function devuelve un numero mayor o igual a cero si la tarea.desc concuerda con alguna descripcion del array listadoPorHacer
	let index = listadoPorHacer.findIndex((tarea) => {
		return tarea.descripcion === descripcion;
	});

	if (index >= 0) {
		listadoPorHacer[index].completado = completado;
		saveDB();
		return true;
	} else {
		//manda el mansaje false (no se hizo)
		return false;
	}
};

const deleteTarea = (descripcion) => {
	cargarDB();

	let nuevoListado = listadoPorHacer.filter(
		(tarea) => tarea.descripcion !== descripcion
	);

	// si el listado por hacer tiene el mismo numero de elementos entoses no se borro
	if (listadoPorHacer.length === nuevoListado.length) {
		return 'No se encontro el objeto a eliminar';
	} else {
		listadoPorHacer = nuevoListado;
		saveDB();
		return "console.log('Eliminacion exitosa')";
	}
};

module.exports = {
	crear,
	getListado,
	actualizar,
	deleteTarea,
};
