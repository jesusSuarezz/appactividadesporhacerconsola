
++++++++++++++++Proyecto realizado del cuso udemy por Jesus Suarez 24/10/18+++++++++++++++++++++++++++++++

Es una app para administrar las actividades que tienes que hacer

Recurda intalar los paquetes de node
```
npm install
```


Recibe comandos por consola

-- Para listar todas tus actividades
node app listar
-- Para listar todas tus actividades que ya completaste
node app listar -c

-- Para crear una nueva tarea por hacer
node app crear -d "Ir al cine en la tarde"

--Para actualizar la tarea
node app actualizar -d "Ir al cine en la tarde" -completo true

-- Para eliminar una tarea
node app borrar -d "Ir al cine en la tarde"
