const { demand } = require('yargs');

const descripcion = {
	demand: true,
	alias: 'd',
	desc: 'Descripción de la tarea por hacer',
};

const completado = {
	default: true,
	alias: 'c',
	desc: 'Marca como completado o pendiente la tarea',
};

const argv = require('yargs')
	.command('listar', 'Lista las tareas', {
		listarCompletas: {
			demand: false,
			alias: 'c',
			desc: 'Lista solo las tareas completas',
		},
	})
	.command('crear', 'Crea una actividad por hacer', {
		descripcion: descripcion,
	})
	.command('actualizar', 'Actualiza el estado a COMPLETADO de una tarea', {
		descripcion: descripcion,
		completado: completado,
	})
	.command(
		'borrar',
		'Elimina la tarea cuando la descripcion concuerda con alguna del array',
		{
			descripcion: descripcion,
		}
	)
	.help().argv;

module.exports = {
	argv,
};
